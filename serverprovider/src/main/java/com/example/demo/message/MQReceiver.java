package com.example.demo.message;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @ClassName MQReceiver
 * @Description: 接收MQ消息
 * @Author zhouyang
 * @Date 2019/6/21 上午10:52.
 */
@Slf4j
@Component
public class MQReceiver {

    //  1.手动创建队列  @RabbitListener(queues = "myQueue")
    // 2.自动创建队列   @RabbitListener(queuesToDeclare = @Queue("myQueue"))
    //3.自动创建队列，Exchange和Queue绑定
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue("myQueue"),
            exchange = @Exchange("myExchange")
    ))
    public void process(String message) {
        log.info("MQReceiver: {}" + message);
    }


    @RabbitListener(bindings = @QueueBinding(
            value = @Queue("order"),
            key = "computer",
            exchange = @Exchange("orderExchange")
    ))
    public void processComputer(String message) {
        log.info("computer MQReceiver: {}" + message);
    }


    @RabbitListener(bindings = @QueueBinding(
            value = @Queue("order"),
            key = "phone",
            exchange = @Exchange("orderExchange")
    ))
    public void processPhone(String message) {
        log.info("phone MQReceiver: {}" + message);
    }
}
