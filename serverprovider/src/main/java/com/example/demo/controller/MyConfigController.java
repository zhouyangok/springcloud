package com.example.demo.controller;

import com.example.demo.config.MyConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName MyConfigController
 * @Description: TODO
 * @Author zhouyang
 * @Date 2019/6/20 下午5:37.
 */

@RestController
@RequestMapping("/config")
public class MyConfigController {

    @Autowired
    private MyConfig myConfig;

    @RequestMapping("/print")
    public String  print(){
        return "name "+myConfig.getName()+" hobbe: "+myConfig.getHobbe();
    }
}
