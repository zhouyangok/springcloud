package com.example.demo.controller;

import com.example.demo.entity.Normal;
import com.example.demo.service.NormalService;
import com.netflix.discovery.converters.Auto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author medo_zy
 * @Desciption:
 * @Date 2019-6-17 15:34
 */
@RestController
@RequestMapping("/server")
public class ServerController {

    @Autowired
    private NormalService normalService;
    
    @RequestMapping("/getServer")
    public List getServer(){
        List list = Arrays.asList(1,2,3);
        return list;
    }

    @RequestMapping("/findAllNormal")
    public List<Normal> findAll(){
        List<Normal> lists = normalService.findAll();
        return lists;
    }
    
}
