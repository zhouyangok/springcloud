package com.example.demo.mapper;

import com.example.demo.entity.Normal;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author medo_zy
 * @Desciption:
 * @Date 2019-6-17 14:46
 */
@Mapper
public interface NormalMapper {

    List <Normal> findAll();

//    int deleteByPrimaryKey(Integer id);
//
    Normal selectByPrimaryKey(Integer id);

}
