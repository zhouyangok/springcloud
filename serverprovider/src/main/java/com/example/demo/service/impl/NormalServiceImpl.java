package com.example.demo.service.impl;

import com.example.demo.entity.Normal;
import com.example.demo.mapper.NormalMapper;
import com.example.demo.service.NormalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName NormalServiceImpl
 * @Description: TODO
 * @Author zhouyang
 * @Date 2019/6/18 上午11:48.
 */
@Service
public class NormalServiceImpl implements NormalService {

    @Autowired
    private NormalMapper normalMapper;

    @Override
    public List<Normal> findAll() {
        return normalMapper.findAll();
    }
}
