package com.example.demo.service;

import com.example.demo.entity.Normal;

import java.util.List;

/**
 * @ClassName NormalService
 * @Description: TODO
 * @Author zhouyang
 * @Date 2019/6/18 上午11:48.
 */

public interface NormalService {

    public List<Normal> findAll();
}
