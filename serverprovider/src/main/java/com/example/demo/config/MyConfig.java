package com.example.demo.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

/**
 * @ClassName MyConfig
 * @Description: TODO
 * @Author zhouyang
 * @Date 2019/6/20 下午5:34.
 */

@Data
@Component
@ConfigurationProperties("author")
@RefreshScope
public class MyConfig {

    private String name;

    private String hobbe;

}
