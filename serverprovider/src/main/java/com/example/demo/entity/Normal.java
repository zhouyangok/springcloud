package com.example.demo.entity;

import lombok.Data;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author medo_zy
 * @Desciption:
 * @Date 2019-6-17 14:36
 */

@Data
public class Normal {

    private Integer id;
    
    private String name;
    
    private String address;
    
    private Integer status;
    
    private Date createTime;
    
    private Date updateTime;
}
