package com.example.demo.message;

import com.example.demo.ServerProviderApplicationTests;
import com.netflix.discovery.converters.Auto;
import org.junit.Test;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @ClassName MQSendTest
 * @Description: TODO
 * @Author zhouyang
 * @Date 2019/6/21 上午10:56.
 */
@Component
public class MQSendTest extends ServerProviderApplicationTests {

    @Autowired
    private AmqpTemplate amqpTemplate;

    @Test
    public void testMqSend() {
        amqpTemplate.convertAndSend("myQueue", "date" + new Date());
    }

    @Test
    public void testOrder() {
        amqpTemplate.convertAndSend("order","computer", "date" + new Date());
    }
}
