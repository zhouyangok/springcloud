package com.example.demo.mapper;


import com.example.demo.ServerProviderApplicationTests;
import com.example.demo.entity.Normal;
import com.example.demo.mapper.NormalMapper;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import org.slf4j.Logger;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author medo_zy
 * @Desciption:
 * @Date 2019-6-17 15:12
 */
@Component
public class NormalMapperTest extends ServerProviderApplicationTests{

    private final static Logger logger = LoggerFactory.getLogger(NormalMapperTest.class);

    @Autowired
   private NormalMapper normalMapper;

    @Test
    public void findAaa(){
      List<Normal> lists = normalMapper.findAll();
      logger.info("输出结果：{}"+lists);
        Assert.assertNotNull(lists.size()>0);
    }
}