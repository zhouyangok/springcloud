package com.example.demo.feignClient;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName ServiceProvider2
 * @Description: TODO
 * @Author zhouyang
 * @Date 2019/6/18 下午3:24.
 */

@FeignClient(value = "SERVERPROVIDER2")
public interface ServiceProvider2 {

    @RequestMapping(value = "/provider2/getList",method = RequestMethod.GET)
    List getServer();
}
