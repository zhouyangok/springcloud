package com.example.demo.feignClient;

import com.example.demo.hystrix.HystrixFallback;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author medo_zy
 * @Desciption:
 * @Date 2019-6-17 16:23
 */
@FeignClient(value = "SERVERPROVIDER",fallback = HystrixFallback.class)
public interface ServiceProvider {


    @RequestMapping(value = "/server/getServer",method = RequestMethod.GET)
    List getServer();

    @RequestMapping(value="/server/findAllNormal",method = RequestMethod.GET)
    List getNormal();

}
