package com.example.demo.service;

import com.example.demo.feignClient.ServiceProvider;
import com.example.demo.feignClient.ServiceProvider2;
import com.netflix.discovery.converters.Auto;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author medo_zy
 * @Desciption:
 * @Date 2019-6-17 15:47
 */
@Service
public class ConsumerService {
    @Autowired
    RestTemplate restTemplate;

    //编译器报错，无视。 因为这个Bean是在程序启动的时候注入的，编译器感知不到，所以报错。
    @Autowired
    ServiceProvider serviceProvider;

    @Autowired
    ServiceProvider2 serviceProvider2;

    @HystrixCommand(fallbackMethod = "getServerBack")
    public List getList() {
//       List list =  restTemplate.getForObject("http://SERVER-PROVIDER/server/getServer",List.class);
        List list = serviceProvider.getServer();
        return list;
    }

    public List getNormal() {
        List list = serviceProvider.getNormal();
        return list;
    }

    public List getProductList() {
        List list = serviceProvider2.getServer();
        return list;
    }

    public List getServerBack(){
        return Arrays.asList("hello","hystrix");
    }
}
