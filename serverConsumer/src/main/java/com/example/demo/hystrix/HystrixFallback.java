package com.example.demo.hystrix;

import com.example.demo.feignClient.ServiceProvider;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

/**
 * @ClassName HystrixFallback
 * @Description: TODO
 * @Author zhouyang
 * @Date 2019/6/18 下午4:12.
 */
@Component
public class HystrixFallback implements ServiceProvider {


    @Override
    public List getServer() {
        return Arrays.asList("hystrix","error");
    }

    @Override
    public List getNormal() {
        return Arrays.asList("hystrix","sorry!");
    }
}
