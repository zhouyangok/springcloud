package com.example.demo.controller;

import com.example.demo.service.ConsumerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author medo_zy
 * @Desciption:
 * @Date 2019-6-17 15:43
 */
@RestController
@RequestMapping("/consumer")
public class ConsumerController {
    
    @Autowired
    private ConsumerService service;
    
    @RequestMapping("/getConsumer")
    public List getConsumer(){
        return service.getList();
    }

    @RequestMapping("/getNormal")
    public List getNormal(){
        return service.getNormal();
    }

    @RequestMapping("/getProductList")
    public List getProductList(){
        return service.getProductList();
    }
    
}
