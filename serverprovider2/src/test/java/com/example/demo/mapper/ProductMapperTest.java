package com.example.demo.mapper;

import com.example.demo.ServerProvider2ApplicationTests;
import com.example.demo.entity.Product;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

import static org.junit.Assert.*;

/**
 * @ClassName ProductMapperTest
 * @Description: TODO
 * @Author zhouyang
 * @Date 2019/6/18 下午2:30.
 */
@Component
public class ProductMapperTest extends ServerProvider2ApplicationTests{

    private final static Logger logger = LoggerFactory.getLogger(ProductMapperTest.class);
    @Autowired
    private ProductMapper productMapper;

    @Test
    public void getList() throws Exception {
       List <Product> list = productMapper.getList();
       logger.info("list={}"+list);
    }

    @Test
    public void selectByPrimaryKey() throws Exception {
    }

}