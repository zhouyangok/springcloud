package com.example.demo.entity;

import lombok.Data;

import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @ClassName Product
 * @Description: TODO
 * @Author zhouyang
 * @Date 2019/6/18 下午2:20.
 */
@Data
@Table
public class Product {

    private Integer id;

    private String name;

    private BigDecimal price;

    private Date createTime;

    private Date updateTime;

    private String detail;

}
