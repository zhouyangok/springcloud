package com.example.demo.service;

import com.example.demo.entity.Product;

import java.util.List;

/**
 * @ClassName ProductService
 * @Description: TODO
 * @Author zhouyang
 * @Date 2019/6/18 下午3:26.
 */

public interface ProductService {

    List<Product> getList();
}
