package com.example.demo.controller;

import com.example.demo.entity.Product;
import com.example.demo.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author medo_zy
 * @Desciption:
 * @Date 2019-6-17 17:06
 */
@RestController
@RequestMapping("/provider2")
public class Provider2Controller {

    @Autowired
    private ProductService service;

    @RequestMapping("/getList")
    public List<Product> getList() {
        List<Product> list = service.getList();
        return list;
    }
}
