package com.example.demo.mapper;

import com.example.demo.entity.Product;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @ClassName ProductMapper
 * @Description: TODO
 * @Author zhouyang
 * @Date 2019/6/18 下午2:22.
 */
@Mapper
public interface ProductMapper {

    List<Product> getList();

    Product selectByPrimaryKey(Integer id);
}
