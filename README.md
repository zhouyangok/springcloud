# springcloud

#### 介绍
springcloud微服务整合实战  
版本说明  
- 1.springboot:2.1.5.RELEASE
- 2.springcloud:Greenwich.SR1
- 3.JDK:1.8
#### 软件架构
软件架构说明  
从零开始，创建4个微服务，包括
- 1.服务注册中心;
- 2.服务提供者server-provider1;
- 3.服务提供者server-provider2;
- 4.服务消费者server-consumer.  
 
服务在注册中心注册后，由消费者请求服务提供者获取数据，服务提供者和数据库交互，进行数据操作。


#### 安装教程

1. git clone 拉取本项目到本地
2. 新建数据库，运行数据库文件cloud.sql，修改各*.yml文件中配置
3. 依次启动注册中心，服务提供者，服务消费者。

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 任务目标  
|  任务名称   | 任务进度  |说明|
|  ----  | ----  |---|
| 创建注册中心  | 已完成 |
| 创建服务提供者一  | 已完成 |
| 创建服务提供者二  | 已完成 |
| 创建服务消费者 | 已完成 |
| 创建高可用配置中心|未完成|使用springcloud bus消息总线进行配置自动更新，使用natapp.cn进行内网穿透，使用webhook进行自动刷新
| 服务间调用  | 已完成 |目前使用feingn在消费者服务中申明接口，需调整到服务提供者中
| hystrxi熔断  | 已完成 |需优化
| 使用gateway服务网关  | 待完成 |



#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request
